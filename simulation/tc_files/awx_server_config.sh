#!/bin/bash

echo "#################################"
echo "  Running awx_server_config.sh "
echo "#################################"

hostnamectl set-hostname awx

useradd cumulus -m -s /bin/bash
echo "cumulus:CumulusLinux!" | chpasswd
echo "cumulus ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10_cumulus

mkdir -p /home/cumulus/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzH+R+UhjVicUtI0daNUcedYhfvgT1dbZXgY33Ibm4MOo+X84Iwuzirm3QFnYf2O3uyZjNyrA6fj9qFE7Ekul4bD6PCstQupXPwfPMjns2M7tkHsKnLYjNxWNql/rCUxoH2B6nPyztcRCass3lIc2clfXkCY9Jtf7kgC2e/dmchywPV5PrFqtlHgZUnyoPyWBH7OjPLVxYwtCJn96sFkrjaG9QDOeoeiNvcGlk4DJp/g9L4f2AaEq69x8+gBTFUqAFsD8ecO941cM8sa1167rsRPx7SK3270Ji5EUF3lZsgpaiIgMhtIB/7QNTkN9ZjQBazxxlNVN6WthF8okb7OSt" >>/home/cumulus/.ssh/authorized_keys

touch /home/cumulus/.ssh/authorized_keys
chown -R cumulus:cumulus /home/cumulus/
chown -R cumulus:cumulus /home/cumulus/.ssh
chmod 700 /home/cumulus/.ssh/
chmod 600 /home/cumulus/.ssh/*

#####Setup SSH key authentication for Ansible

yum -y install wget

cat <<EOT > /home/cumulus/ansible_keys.sh 
#!/bin/bash
sleep 60
wget -O - http://192.168.200.1/authorized_keys >> /home/cumulus/.ssh/authorized_keys
EOT

chmod +x /home/cumulus/ansible_keys.sh
chown cumulus:cumulus /home/cumulus/ansible_keys.sh

cat <<EOT > /etc/rc.d/rc.local 
#!/bin/bash
/home/cumulus/ansible_keys.sh
EOT

chmod +x /etc/rc.d/rc.local
systemctl enable rc-local
systemctl start rc-local

echo "Applying network config"

cat <<EOT > /etc/sysconfig/network-scripts/ifcfg-eth0
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="no"
NAME="eth0"
UUID="bc4d7962-deb0-4c5d-97d0-6657f006f9ea"
DEVICE="eth0"
ONBOOT="yes"
IPADDR="192.168.200.251"
PREFIX="24"
GATEWAY="192.168.200.1"
DNS1="192.168.200.1"
DNS2="1.0.0.1"
DOMAIN="cumulusnetworks.com"
EOT

sed -i 's|SELINUX=enforcing|SELINUX=disabled|g' /etc/selinux/config
setenforce 0

systemctl disable firewalld
systemctl stop firewalld
yum install -y epel-release
yum-config-manager --add-repo https://releases.ansible.com/ansible-tower/cli/ansible-tower-cli-el7.repo
yum install -y yum-utils device-mapper-persistent-data lvm2 ansible git python-devel python-pip vim-enhanced gcc python3
yum install ansible-tower-cli --nogpgcheck
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce -y
pip3 install ansible-tower-cli
pip install --upgrade pip
pip install docker-compose
pip install docker

systemctl start docker
systemctl enable docker

git clone https://github.com/ansible/awx.git
git clone https://github.com/ansible/awx-logos.git
cd awx/installer

cat <<EOT > /home/vagrant/awx/installer/inventory

localhost ansible_connection=local ansible_python_interpreter="/usr/bin/env python"
[all:vars]
dockerhub_base=ansible
awx_task_hostname=awx
awx_web_hostname=awxweb
postgres_data_dir=/var/lib/pgdocker
host_port=80
host_port_ssl=443
docker_compose_dir="~/.awx/awxcompose"
pg_username=awx
pg_password=awxpass
pg_database=awx
pg_port=5432
admin_user=admin
admin_password=cumulus
create_preload_data=True
secret_key=cumulus
awx_official=true
awx_alternate_dns_servers="192.168.200.1"
project_data_dir=/var/lib/awx/projects
EOT

ansible-playbook -i inventory install.yml -vv

echo "#################################"
echo "   Finished"
echo "#################################"
