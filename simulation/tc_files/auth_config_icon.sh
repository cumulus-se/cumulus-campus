#!/bin/bash

echo "#################################"
echo "  Running auth_config.sh "
echo "#################################"

# cosmetic fix for dpkg-reconfigure: unable to re-open stdin: No file or directory during vagrant up
export DEBIAN_FRONTEND=noninteractive

useradd cumulus -m -s /bin/bash
echo "cumulus:CumulusLinux!" | chpasswd
echo "cumulus ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10_cumulus

echo "Disable DNSSEC"
sed -i 's/DNSSEC=yes/DNSSEC=no/' /etc/systemd/resolved.conf 
systemctl restart systemd-resolved.service

echo "Update apt"
apt-get update -qy 

echo "Install LLDP"
apt-get install -qy lldpd
echo "configure lldp portidsubtype ifname" > /etc/lldpd.d/port_info.conf

echo "Install pip3"
apt-get install -qy python3-pip

echo "Enabling LLDP"
/lib/systemd/systemd-sysv-install enable lldpd
systemctl start lldpd.service

#####Setup SSH key authentication for Ansible
mkdir -p /home/cumulus/.ssh
cat <<EOT > /etc/rc.local 
#!/bin/bash
#
wget -O /home/cumulus/.ssh/authorized_keys http://192.168.200.1/authorized_keys
chown -R cumulus:cumulus /home/cumulus/.ssh

EOT

chmod +x /etc/rc.local

echo "retry 1;" >> /etc/dhcp/dhclient.conf
echo "timeout 600;" >> /etc/dhcp/dhclient.conf
#Disable autoupdate
echo "Disabling Autoupdates"
echo -e "APT::Periodic::Update-Package-Lists "0";" > /etc/apt/apt.conf.d/10periodic
echo -e "APT::Periodic::Download-Upgradeable-Packages "0";" >> /etc/apt/apt.conf.d/10periodic
echo -e "APT::Periodic::AutocleanInterval "0";" >> /etc/apt/apt.conf.d/10periodic


echo "Applying network config" 

echo "8021q" >> /etc/modules

# remap hasn't occurred yet so do not apply settings yet.
cat <<EOT > /etc/netplan/01-netcfg.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: false
      addresses: [192.168.200.252/24]
      gateway4: 192.168.200.1
      nameservers:
        addresses: [192.168.200.1,4.2.2.2]
    eth1:
      dhcp4: true
    vagrant:
      dhcp4: true
  vlans:
    vlan10:
      accept-ra: false
      id: 10
      link: eth1
      dhcp4: false
      addresses: [10.223.0.253/24]
    vlan20:
      accept-ra: false
      id: 20
      link: eth1
      dhcp4: false
      addresses: [10.223.1.253/24]
    vlan30:
      accept-ra: false
      id: 30
      link: eth1
      dhcp4: false
      addresses: [10.223.2.253/24]
    vlan40:
      accept-ra: false
      id: 40
      link: eth1
      dhcp4: false
      addresses: [10.223.3.253/24]

EOT

echo "Install FreeRADIUS server for clients"
apt-get install -qy freeradius
systemctl stop freeradius

cat <<EOT > /etc/freeradius/3.0/mods-available/eap
eap {
        default_eap_type = peap
        timer_expire     = 60
        ignore_unknown_eap_types = no
        cisco_accounting_username_bug = no
        max_sessions = 8
        md5 {
        }
        leap {
        }
        gtc {
                auth_type = PAP
        }
        tls-config tls-common {
                private_key_password = whatever
                private_key_file = /etc/ssl/private/ssl-cert-snakeoil.key
                certificate_file = /etc/ssl/certs/ssl-cert-snakeoil.pem
                ca_file = /etc/ssl/certs/ca-certificates.crt
                cipher_list = "DEFAULT"
                cipher_server_preference = no
                ecdh_curve = "prime256v1"
                cache {
                        enable = no
                        lifetime = 24 # hours
                }
                verify {
                }
                ocsp {
                        enable = no
                        override_cert_url = yes
                        url = "http://127.0.0.1/ocsp/"
                }
        }
        tls {
                tls = tls-common
        }
        ttls {
                tls = tls-common
                default_eap_type = md5
                copy_request_to_tunnel = no
                use_tunneled_reply = no
                virtual_server = "inner-tunnel"
        }
        peap {
                tls = tls-common
                default_eap_type = mschapv2
                copy_request_to_tunnel = no
                use_tunneled_reply = yes
                virtual_server = "inner-tunnel"
        }
        mschapv2 {
        }
}

EOT

cat <<EOT > /etc/freeradius/3.0/mods-available/files
files {
	filename = /etc/freeradius/3.0/mods-config/files/authorize
	acctusersfile = /etc/freeradius/3.0/mods-config/files/accounting
	preproxy_usersfile = /etc/freeradius/3.0/mods-config/files/pre-proxy
}
files authorized_macs {
        key = "%{Calling-Station-ID}"
        usersfile = /etc/freeradius/3.0/mods-config/files/authorized_macs
}
files authorized_cli {
        usersfile = /etc/freeradius/3.0/mods-config/files/authorized_cli
}

EOT

cat <<EOT > /etc/freeradius/3.0/mods-config/files/authorized_macs
44-38-39-00-00-68
	Reply-Message = "Device with MAC Address %{Calling-Station-Id} authorized for network access"
44-38-39-00-00-A8
	Reply-Message = "Device with MAC Address %{Calling-Station-Id} authorized for network access"

EOT

cat <<EOT > /etc/freeradius/3.0/mods-config/files/authorized_cli
test-radius     Cleartext-Password := "cumulus"
                Service-Type = Administrative-User,
                Cisco-AVPair = "shell:roles=network-administrator",
                Cisco-AVPair += "shell:priv-lvl=15"

EOT


cat <<EOT > /etc/freeradius/3.0/mods-config/files/authorize
client01	Cleartext-Password := "cumulus"
		Tunnel-Type = "VLAN",
		Tunnel-Medium-Type = "IEEE-802",
		Tunnel-Private-Group-Id = "10"
client02        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "10"
client03        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "40"
client04        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "30"
client05        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "10"
client06        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "10"
client07        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "40"
client08        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "30"
client09        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "30"
client10        Cleartext-Password := "cumulus"
                Tunnel-Type = "VLAN",
                Tunnel-Medium-Type = "IEEE-802",
                Tunnel-Private-Group-Id = "10"

DEFAULT	Framed-Protocol == PPP
	Framed-Protocol = PPP,
	Framed-Compression = Van-Jacobson-TCP-IP
DEFAULT	Hint == "CSLIP"
	Framed-Protocol = SLIP,
	Framed-Compression = Van-Jacobson-TCP-IP
DEFAULT	Hint == "SLIP"
	Framed-Protocol = SLIP

EOT

cat <<EOT > /etc/freeradius/3.0/policy.conf
rewrite_calling_station_id {
        if (Calling-Station-Id =~ /([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})/i) {
                update request {
                        Calling-Station-Id := "%{tolower:%{1}-%{2}-%{3}-%{4}-%{5}-%{6}}"
                }
        }
        else {
                noop
        }
}

EOT

cat <<EOT > /etc/freeradius/3.0/sites-available/default
server default {
listen {
	type = auth
	ipaddr = *
	port = 0
	limit {
	      max_connections = 16
	      lifetime = 0
	      idle_timeout = 30
	}
}
listen {
	ipaddr = *
	port = 0
	type = acct
	limit {
	}
}
listen {
	type = auth
	ipv6addr = ::	# any.  ::1 == localhost
	port = 0
	limit {
	      max_connections = 16
	      lifetime = 0
	      idle_timeout = 30
	}
}
listen {
	ipv6addr = ::
	port = 0
	type = acct
	limit {
	}
}
authorize {
	filter_username
	preprocess
	if (Service-Type == Authenticate-Only) {
		authorized_cli
                if (!ok) {
                        reject
                }
                else {
                        update control {
                                Auth-Type := Accept
                        }
                }
        }
	elsif (!EAP-Message) {
		rewrite_calling_station_id
                authorized_macs
                if (!ok) {
                        reject
                }
                else {
                        update control {
                                Auth-Type := Accept
                        }
                }
        }
        else {
                eap
        }
	mschap
	ntdomain
	eap {
		ok = return
	}
	files
	expiration
	logintime
}
authenticate {
	Auth-Type MS-CHAP {
		mschap
	}
	mschap
	eap
}
preacct {
	preprocess
	acct_unique
	suffix
	files
}
accounting {
	detail
	unix
	-sql
	exec
	attr_filter.accounting_response
}
session {
}
post-auth {
	if (Calling-Station-Id =~ /^44-38-39-00-00-68/i) {
			update reply {
					cisco-avpair += "device-traffic-class=voice"
			}
	}
	update {
		&reply: += &session-state:
	}
	-sql
	exec
	remove_reply_message_if_eap
	Post-Auth-Type REJECT {
		-sql
		attr_filter.access_reject
		eap
		remove_reply_message_if_eap
	}
	Post-Auth-Type Challenge {
	}
}
pre-proxy {
}
post-proxy {
	eap
}
}

EOT

cat <<EOT > /etc/freeradius/3.0/sites-available/inner-tunnel
server inner-tunnel {
listen {
       ipaddr = 127.0.0.1
       port = 18120
       type = auth
}
authorize {
	filter_username
	chap
	mschap
	ntdomain
	update control {
		&Proxy-To-Realm := LOCAL
	}
	eap {
		ok = return
	}
	files
	expiration
	logintime
	pap
}
authenticate {
	Auth-Type PAP {
		pap
	}
	Auth-Type CHAP {
		chap
	}
	Auth-Type MS-CHAP {
		mschap
	}
	mschap
	eap
}
session {
	radutmp
}
post-auth {
	if (0) {
		update reply {
			User-Name !* ANY
			Message-Authenticator !* ANY
			EAP-Message !* ANY
			Proxy-State !* ANY
			MS-MPPE-Encryption-Types !* ANY
			MS-MPPE-Encryption-Policy !* ANY
			MS-MPPE-Send-Key !* ANY
			MS-MPPE-Recv-Key !* ANY
		}
		update {
			&outer.session-state: += &reply:
		}
	}
	Post-Auth-Type REJECT {
		-sql
		attr_filter.access_reject
		update outer.session-state {
			&Module-Failure-Message := &request:Module-Failure-Message
		}
	}
}
pre-proxy {
}
post-proxy {
	eap
}
} # inner-tunnel server block

EOT

cat <<EOT > /etc/freeradius/3.0/clients.conf

client localhost {
	ipaddr = 127.0.0.1
	proto = *
	secret = testing123
	require_message_authenticator = no
	nas_type	 = other	# localhost isn't usually a NAS...
	limit {
		max_connections = 16
		lifetime = 0
		idle_timeout = 30
	}
}
client localhost_ipv6 {
	ipv6addr	= ::1
	secret		= testing123
}
client cumulus {
	ipaddr	= 192.168.200.0/24
	secret	= cumulus
	nastype = cisco
}

EOT

systemctl start freeradius



echo "Install DHCP server for clients"
apt-get install -qy isc-dhcp-server

cat <<EOT > /etc/dhcp/dhcpd.conf
option domain-name "demo.cumulusnetworks.com";
option domain-name-servers 1.1.1.1;
default-lease-time 600;
max-lease-time 7200;
ddns-update-style none;

subnet 10.255.1.0 netmask 255.255.255.0 {
}
subnet 192.168.200.0 netmask 255.255.255.0 {
}
subnet 10.223.0.0 netmask 255.255.255.0 {
  range 10.223.0.200 10.223.0.240;
  option routers 10.223.0.254;
}
subnet 10.223.1.0 netmask 255.255.255.0 {
  range 10.223.1.200 10.223.1.240;
  option routers 10.223.1.254;
}
subnet 10.223.2.0 netmask 255.255.255.0 {
  range 10.223.2.200 10.223.2.240;
  option routers 10.223.2.254;
}
subnet 10.223.3.0 netmask 255.255.255.0 {
  range 10.223.3.200 10.223.3.240;
  option routers 10.223.3.254;
}
EOT
systemctl enable isc-dhcp-server
systemctl start isc-dhcp-server

#tell the service that waits for interfaces to be up to ignore eth1 and vagrant
#it makes a reboot take a long time
sed -i "s/systemd-networkd-wait-online/systemd-networkd-wait-online --ignore=eth1 --ignore=vagrant/" /lib/systemd/system/systemd-networkd-wait-online.service

echo "Change console resolution"
echo 'GRUB_GFXPAYLOAD_LINUX=keep' >>/etc/default/grub
echo 'GRUB_GFXMODE=720x400'  >>/etc/default/grub
echo 'GRUB_CMDLINE_LINUX_DEFAULT="nomodeset"' >>/etc/default/grub
update-grub 2>&1 #creates red output in vagrant


echo "#################################"
echo "   Finished"
echo "#################################"
