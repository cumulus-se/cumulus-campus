#!/bin/bash

echo "#################################"
echo "  Running awx_server_config.sh "
echo "#################################"

hostnamectl set-hostname awx

useradd cumulus -m -s /bin/bash
echo "cumulus:CumulusLinux!" | chpasswd
echo "cumulus ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10_cumulus

mkdir -p /home/cumulus/.ssh
# echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwWf8lRxxwxHxp65pkOPy1YmbBuDWIcF0byt6C1IAz8u6uGGcDMR8v2VgrxslEcWTgtYJrnXGCbcq8bTe4isYmAQzXzDZ8Lmc1kZIYbhtrNEDIUEAjIylMLTLF9gy4Mn0GUqrBWXGhxLI1y7ZWgAH0FqoW1sGgfp5GVKrSDYW/MWKOXqzL23mpVqmpmPAAIrQHXP+xjpHbxNxlXaEQUaTfgrvaj49KOZR0DB5wgZkZ3xcJe5MkZ+YYbSbv81/t8LbTYtVqLNUqoo3Eage4AgD4jIlAqZ/pC6tMKyL+ySduCvOJA5i00ijIepuBji5arMqwDV6HCb/k4DD6ueqwywpN vagrant" >>/home/cumulus/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzH+R+UhjVicUtI0daNUcedYhfvgT1dbZXgY33Ibm4MOo+X84Iwuzirm3QFnYf2O3uyZjNyrA6fj9qFE7Ekul4bD6PCstQupXPwfPMjns2M7tkHsKnLYjNxWNql/rCUxoH2B6nPyztcRCass3lIc2clfXkCY9Jtf7kgC2e/dmchywPV5PrFqtlHgZUnyoPyWBH7OjPLVxYwtCJn96sFkrjaG9QDOeoeiNvcGlk4DJp/g9L4f2AaEq69x8+gBTFUqAFsD8ecO941cM8sa1167rsRPx7SK3270Ji5EUF3lZsgpaiIgMhtIB/7QNTkN9ZjQBazxxlNVN6WthF8okb7OSt" >>/home/cumulus/.ssh/authorized_keys

touch /home/cumulus/.ssh/authorized_keys
chown -R cumulus:cumulus /home/cumulus/
chown -R cumulus:cumulus /home/cumulus/.ssh
chmod 700 /home/cumulus/.ssh/
chmod 600 /home/cumulus/.ssh/*
# chown cumulus:cumulus /home/cumulus/.ssh/*


#####Setup Ansible awx Dependencies
yum -y install wget

#####Setup SSH key authentication for Ansible

systemctl enable rc-local
systemctl start rc-local
chmod +x /etc/rc.d/rc.local

cat <<EOT > /home/cumulus/ansible_keys.sh 
#!/bin/bash
sleep 60
wget -O - http://192.168.200.1/authorized_keys >> /home/cumulus/.ssh/authorized_keys
EOT
chmod +x /home/cumulus/ansible_keys.sh
chown cumulus:cumulus /home/cumulus/ansible_keys.sh

cat <<EOT > /etc/rc.d/rc.local 
#!/bin/bash
/home/cumulus/ansible_keys.sh
EOT

echo "Applying network config"

cat <<EOT > /etc/sysconfig/network-scripts/ifcfg-eth0
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="no"
NAME="eth0"
UUID="bc4d7962-deb0-4c5d-97d0-6657f006f9ea"
DEVICE="eth0"
ONBOOT="yes"
IPADDR="192.168.200.251"
PREFIX="24"
GATEWAY="192.168.200.1"
DNS1="192.168.200.1"
DNS2="1.0.0.1"
DOMAIN="cumulusnetworks.com"
EOT


echo "#################################"
echo "   Finished"
echo "#################################"
