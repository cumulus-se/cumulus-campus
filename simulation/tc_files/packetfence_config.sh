#!/bin/bash

echo "#################################"
echo "  Running packetfence_config.sh "
echo "#################################"

# cosmetic fix for dpkg-reconfigure: unable to re-open stdin: No file or directory during vagrant up
export DEBIAN_FRONTEND=noninteractive

useradd cumulus -m -s /bin/bash
echo "cumulus:CumulusLinux!" | chpasswd
echo "cumulus ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10_cumulus

echo "Disable DNSSEC"
sed -i 's/DNSSEC=yes/DNSSEC=no/' /etc/systemd/resolved.conf 
systemctl restart systemd-resolved.service

echo "Update apt"
apt-get update -qy 

echo "Install LLDP"
apt-get install -qy lldpd
echo "configure lldp portidsubtype ifname" > /etc/lldpd.d/port_info.conf

echo "Install pip3"
apt-get install -qy python3-pip

echo "Enabling LLDP"
/lib/systemd/systemd-sysv-install enable lldpd
systemctl start lldpd.service

#####Setup SSH key authentication for Ansible
mkdir -p /home/cumulus/.ssh
cat <<EOT > /etc/rc.local 
#!/bin/bash
#
wget -O /home/cumulus/.ssh/authorized_keys http://192.168.200.1/authorized_keys
chown -R cumulus:cumulus /home/cumulus/.ssh

echo never > /sys/kernel/mm/transparent_hugepage/enabled

EOT

echo -e "vm.overcommit_memory = 1" >> /etc/sysctl.conf

chmod +x /etc/rc.local

echo "retry 1;" >> /etc/dhcp/dhclient.conf
echo "timeout 600;" >> /etc/dhcp/dhclient.conf
#Disable autoupdate
echo "Disabling Autoupdates"
echo -e "APT::Periodic::Update-Package-Lists "0";" > /etc/apt/apt.conf.d/10periodic
echo -e "APT::Periodic::Download-Upgradeable-Packages "0";" >> /etc/apt/apt.conf.d/10periodic
echo -e "APT::Periodic::AutocleanInterval "0";" >> /etc/apt/apt.conf.d/10periodic


echo "Applying network config" 

echo "8021q" >> /etc/modules

# remap hasn't occurred yet so do not apply settings yet.
cat <<EOT > /etc/network/interfaces
auto eth0
iface eth0 inet static
  address  192.168.200.252
  netmask 255.255.255.0
  gateway 192.168.200.1
  dns-nameservers 1.1.1.1

auto eth1.2
iface eth1.2 inet static
  address  10.1.2.253
  netmask 255.255.255.0
  vlan-raw-device eth1

auto eth1.3
iface eth1.3 inet static
  address  10.1.3.253
  netmask 255.255.255.0
  vlan-raw-device eth1

auto eth1.10
iface eth1.10 inet static
  address  10.1.10.253
  netmask 255.255.255.0
  vlan-raw-device eth1

auto eth1.20
iface eth1.20 inet static
  address  10.1.20.253
  netmask 255.255.255.0
  vlan-raw-device eth1

auto eth1.30
iface eth1.30 inet static
  address  10.1.30.253
  netmask 255.255.255.0
  vlan-raw-device eth1

auto eth1.40
iface eth1.40 inet static
  address  10.1.40.253
  netmask 255.255.255.0
  vlan-raw-device eth1

EOT

#tell the service that waits for interfaces to be up to ignore eth1 and vagrant
#it makes a reboot take a long time
sed -i "s/systemd-networkd-wait-online/systemd-networkd-wait-online --ignore=eth1 --ignore=vagrant/" /lib/systemd/system/systemd-networkd-wait-online.service

echo "Change console resolution"
echo 'GRUB_GFXPAYLOAD_LINUX=keep' >>/etc/default/grub
echo 'GRUB_GFXMODE=720x400'  >>/etc/default/grub
echo 'GRUB_CMDLINE_LINUX_DEFAULT="nomodeset"' >>/etc/default/grub
update-grub 2>&1 #creates red output in vagrant


echo "#################################"
echo "   Finished"
echo "#################################"
