#!/bin/bash
vagrant up oob-mgmt-server oob-mgmt-switch auth
sleep 10
vagrant up /leaf/ /spine/ /exit/
vagrant up /closet/ /client/
vagrant up
