#!/bin/bash

set -x

cd ./simulation_$CI_PROJECT_NAME

date
vagrant destroy -f /server0/ /leaf/ /spine/ /border/ /fw/

#check that we aren't on cumulus-campus/master.* project else this will not work because
#the machines are halted from disk image creation
if [ "$CI_PROJECT_NAME" != 'cumulus-campus' ]
then
  echo "Not on cumulus-campus NetQ decomm servers"
  vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"
else
  echo "On cumulus-campus. Check if master.*"
  if [[ "$CI_COMMIT_BRANCH" =~ master ]]
  then
    echo "On cumulus-campus/master. Do nothing"
  else
    echo "Not on cumulus-campus/master, we can netq decomm"
    vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"
  fi
fi


date
vagrant destroy -f netq-ts oob-mgmt-server oob-mgmt-switch

echo "Finished."
