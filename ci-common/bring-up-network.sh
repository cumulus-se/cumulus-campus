#!/bin/bash
#

set -x

# Force Colored output for Vagrant when being run in CI Pipeline
export VAGRANT_FORCE_COLOR=true

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Could not bring up last series of devices, there was an error of some kind!"
    exit 1
fi
}

#echo "Currently in directory: $(pwd)"

cd simulation_$CI_PROJECT_NAME

echo "#########################################"
echo "#   Collecting Bootable Vagrant Nodes   #"
echo "#########################################"
VAGRANT_NODES=($(vagrant status | grep "not created" | awk '{print $1}'))
echo ""
for i in ${VAGRANT_NODES[@]}; do echo " - $i"; done
echo ""

echo "#########################################"
echo "#   Booting Simulated  Infrastructure   #"
echo "#########################################"
Z=1
for ((i=0; i<${#VAGRANT_NODES[@]}; i+=4))
do
	PART=( "${VAGRANT_NODES[@]:i:4}" )
	echo "+---------------------------------------+"
	echo "|   Boot Group ($Z) Devices:             |"
	echo "+---------------------------------------+"
	for x in ${PART[@]}; do echo "   - $x "; done
	echo ""
	vagrant up ${PART[*]}
	echo ""
	((Z+=1))
	check_state
done

echo "#####################################"
echo "#   Status of all simulated nodes   #"
echo "#####################################"
vagrant status

exit 0

exit 0
