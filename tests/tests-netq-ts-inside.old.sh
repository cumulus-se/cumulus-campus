#!/bin/bash

set -x
set -e

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Previous command failed test!"
    exit 1
fi
}
echo "Agent register & NetQ processing time"
sleep 60

echo "netq show agents"
netq show agents 

echo "netq show inventory br"
netq show inventory br

echo "netq check agents include 0"
netq check agents include 0

echo "netq check interfaces"
netq check interfaces include 0
netq check interfaces include 1
netq check interfaces include 2
netq check interfaces include 3

echo "netq check mtu"
netq check mtu include 0
netq check mtu include 1
netq check mtu include 2

echo "netq check vlan"
netq show vlan
netq check vlan include 0
netq check vlan include 1

echo "netq check ntp"
netq show ntp
netq check ntp include 0
