#!/bin/bash
#

set -x

# Force Colored output for Vagrant when being run in CI Pipeline
export VAGRANT_FORCE_COLOR=true

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Could not bring up last series of devices, there was an error of some kind!"
    exit 1
fi
}

#echo "Currently in directory: $(pwd)"

cd simulation

echo "#########################################"
echo "#   Starting PoP Infra Switches...  #"
echo "#########################################"
vagrant up pop-ed1 pop-ed2 pop-ex1 pop-ex2 pop-sp1 pop-sp2
check_state
vagrant up pop-a2-le1 pop-a2-le2 pop-z09-le1 pop-z09-le2
check_state

echo "#####################################"
echo "#   Starting PoP Additional Network Devices #"
echo "#####################################"
vagrant up pop-mac1 pop-mac2 pop-lb1 pop-lb2
check_state
#vagrant up dcr1 dcr2
check_state

echo "#####################################"
echo "#   Starting PoP1 Hosts            .#"
echo "#####################################"
#vagrant up log-pp1-sys-p0 hv-pp1-ld-p0-service hv-pp1-ld-p0-storage hv-pp1-ld-p1-service hv-pp1-ld-p1-storage ntp-pp1-p0
#check_state

echo "#####################################"
echo "#   Starting PoP2 Hostss...         #"
echo "#####################################"
#vagrant up log-pp2-sys-p0 hv-pp2-ld-p2-service hv-pp2-ld-p2-storage hv-pp2-ld-p3-service hv-pp2-ld-p3-storage ntp-pp2-p0
#check_state

echo "#####################################"
echo "#   Status of all simulated nodes   #"
echo "#####################################"
vagrant status

exit 0
