#!/bin/bash

set -e
set -x

function error() {
  echo -e "\e[0;33mERROR: Provisioning of the simulation failed while running the command $BASH_COMMAND at line $BASH_LINENO.\e[0m" >&2
  if [ "$debug" != "true" ]; then
    echo " >>>Destroying Simulation<<<"
    vagrant destroy -f
  fi
  exit 1
}

trap error ERR

source ./tests/pipeline_failure_behavior

echo "Starting to run tests...."

cd simulation

# Force Colored output for Vagrant when being run in CI Pipeline
export VAGRANT_FORCE_COLOR=true

# tests performed from the oob-server
vagrant ssh oob-mgmt-server -c "bash /home/cumulus/tests/tests-oob-server-inside.sh"
