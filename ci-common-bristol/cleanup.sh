#!/bin/bash

set -x

cd simulation

#destroy all network nodes first so when we netq decomm, they don't reappear due to order of destroy with the netq-ts
date
vagrant destroy -f /pop/

#decommission nodes from netq
vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"

#destroy the rest of the oob-mgmt
date
vagrant destroy -f netq-ts oob-mgmt-server oob-mgmt-switch

echo "Finished."
