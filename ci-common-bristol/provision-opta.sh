#!/bin/bash

set -e

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR on previous command - Exit with failure"
    exit 1
fi
}

cd simulation

echo "NetQ Master Boostrap...takes several minutes"
#fetch this URL from topology-pop.dot
BOOTSTRAP_TARBALL_URL=`cat topology-pop.dot | grep NETQ_BOOTSTRAP_TARBALL_URL | awk 'BEGIN { FS = "=" } ; { print $2 }' | tr -d '"'` 
vagrant ssh netq-ts -c "netq bootstrap master interface eth0 tarball $BOOTSTRAP_TARBALL_URL"

echo "made it past bootstrap"

echo "NetQ OPTA Install...takes several more minutes"
INSTALL_TARBALL_URL=`cat topology-pop.dot | grep NETQ_OPTA_TARBALL_URL | awk 'BEGIN { FS = "=" } ; { print $2 }' | tr -d '"'`
vagrant ssh netq-ts -c "netq install opta standalone full interface eth0 bundle $INSTALL_TARBALL_URL config-key $NETQ_CONFIG_KEY"

# For multi-site cloud deployments, a site dedicated for CI is required
# A premise name must be specificied in the CI config. This is configured as an environment variable in gitlab CI settings.
echo "Adding NetQ CLI Server"
PREMISE_NAME=`cat topology-pop.dot | grep NETQ_PREMISE_NAME | awk 'BEGIN { FS = "=" } ; { print $2 }' | tr -d '"'`
vagrant ssh netq-ts -c "netq config add cli server api.netq.cumulusnetworks.com access-key $NETQ_ACCESS_KEY secret-key $NETQ_SECRET_KEY premise $PREMISE_NAME port 443"

echo "Restarting NetQ agent and cli"
vagrant ssh netq-ts -c "netq config restart cli"
vagrant ssh netq-ts -c "netq config restart agent"

#cleanup step prior to testing in case a previous pipline failure occurred
#echo "Performing NetQ agent decommission"
#vagrant ssh netq-ts -c "bash /home/cumulus/tests/netq-decommission-inside.sh"
