# Cumulus Networks Campus Deployment

This repository holds all the configurations for a standardized deployment using the following technologies:

* Network
  * Layer 2 VLANs
  * VxLAN / EVPN
  * Leaf/Spine + Closet switches

* 802.1x
  * DHCP / DHCP Relay
  * NAC via FreeRADIUS
  * Username/Password and MAC Auth
  * Voice VLANs

* Management
  * Ansible AWX
  * Cumulus NetQ
  * Grafana
  * Prometheus
  * Prometheus node_exporter on cumulus switches
  * Rsyslog / NTP / RADIUS Auth to central server

## Setup

You may clone this repository to a server with KVM, libvirt and Vagrant and run it locally. To clone the repo and start the simulation, run the following:

```bash
git clone https://gitlab.com/cumulus-se/cumulus-campus.git
cd cumulus-campus/simulation
./startup.sh
```

Once completed, ssh to the oob-mgmt-server to run an ansible playbook that will configure the network devices and hosts. *Note:* Only the oob-mgmt-server has external connectivity. vagrant ssh will not work to any other device.

```bash
vagrant ssh oob-mgmt-server
./turnup.sh
```

### Using NetQ

The NetQ appliance is not required for this environment. If you have access to the NetQ cloud service and the required resources, you can provision the NetQ appliance with the following steps:

1. Download the NetQ Cloud installer files and copy them to the netq-ts VM:

* Login to https://cumulusnetworks.com/downloads/
* Select _NetQ --> Version 3.0.0 (or newer) --> Bootstrap_ and download the file
* Select _NetQ --> Version 3.0.0 (or newer) --> Appliance (Cloud)_ and download the file
* Copy the files to the netq-ts VM:

    ```bash
    cd cumulus-campus/simulation
    vagrant plugin install scp
    vagrant scp netq-bootstrap-3.0.0.tgz netq-ts:/home/vagrant/
    vagrant scp NetQ-3.0.0-opta.tgz netq-ts:/home/vagrant/
    ```

* Login to the netq-ts server and move the files in to place:

    ```bash
    vagrant ssh oob-mgmt-server
    ssh netq-ts
    sudo mv /home/vagrant/netq-bootstrap-3.0.0.tgz /mnt/installables/
    sudo mv /home/vagrant/NetQ-3.0.0-opta.tgz /mnt/installables/
    ```

2. Add your customer-specific NetQ Cloud config-key to the following file:

```nano cumulus-campus/automation/roles/netq_server/vars/netq_config_key.yml```

Replace the text _insert_netq_config_key_here_ with your NetQ config-key. The config-key is a 56 character string emailed to you by a Cumulus representative and looks similar to the following: `CNDEExIhY3ducHJvZDIubmV0cS5jdW11bHVzbmV1d29fa4MuY30tGLsD`

3. Run the full deploy playbook to setup NetQ:

    ```bash
    ssh oob-mgmt-server
    cd cumulus-campus
    ansible-playbook playbooks/deploy_all.yml -i inventories/demo/
    ```

## Network Inventory

The following network topology is provided in this repo:

`inventories/demo`
![Campus Topology](documentation/campus_topo.png)

* 16 Closet Switches
* VLANs 10,20,30,40

## Feature Tests

### RADIUS CLI Auth

`ssh test-radius@leaf01`
Password: cumulus

### PTM

`ansible leaf:spine:exit:closet -a "ptmctl --lldp -d"`

## Network Notes

* Leaf switch ports can either be access interfaces, or trunks to a closet
* Closet switch interfaces can either be access interfaces on a specific VLAN, or 802.1x interfaces with dynamic VLAN and IP address assignment. There is a RADIUS and DHCP server running on the server called "auth"
* Closet switches are running MLAG to connect to a pair of leaf switches

## Outstanding Issues

* In `automation/roles/grafana/main.yml` the task `import grafana dashboard` is commented out due to an ansible 2.9.6 bug. The bug is fixed in dev code, but not in a public release yet
